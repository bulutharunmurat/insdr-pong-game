(function () {
    var CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)'
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 0,
            left: 350,
            borderRadius: 50,
            background: '#C6A62F'
        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F'
        },
        stick1: {
            left: 0,
            top: 257.5
        },
        stick2: {
            left: 888,
            top: 257.5
        },
        score:{
            color: "#ffffff",
            position: "absolute",  
        },
        score1: {
            left:400,
            top:100
        },
        score2: {
            left:500,
            top:100
        }
    };

    var CONSTS = {
    	gameSpeed: 8,
        score1: 0,
        score2: 0,
        stick1Speed: 1,
        stick2Speed: 1,
        ballTopSpeed: 30,
        ballLeftSpeed: 100,
        ball1TopSpeed: 20,
        ball1LeftSpeed: 50
    };
    
    function start() {
        draw();
        setEvents();
        roll();
        loop();
        readScores();
    }

    function draw() {
        $('<div/>', {id: 'pong-game'}).css(CSS.arena).appendTo('body');
        $('<div/>', {id: 'pong-line'}).css(CSS.line).appendTo('#pong-game');
        $('<div/>', {id: 'pong-ball'}).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', {id: 'stick-1'}).css($.extend(CSS.stick1, CSS.stick)).appendTo('#pong-game');
        $('<div/>', {id: 'stick-2'}).css($.extend(CSS.stick2, CSS.stick)).appendTo('#pong-game');
        $('<div/>', {id: 'score-1'}).css($.extend(CSS.score1, CSS.score)).appendTo('#pong-game');
        $('<div/>', {id: 'score-2'}).css($.extend(CSS.score2, CSS.score)).appendTo('#pong-game');
        $("#score-1")[0].innerHTML = CONSTS.score1; 
        $("#score-2")[0].innerHTML = CONSTS.score1;
    }

    function setEvents() {
      
        $(document).on('keydown', function (e) {
            if (e.keyCode == 80) {
                saveGame();
            }
        });
    }
    
    function loop() {
        window.pongLoop = setInterval(function () {
            
            CSS.ball.top += CONSTS.ballTopSpeed;
            CSS.ball.left += CONSTS.ballLeftSpeed;
           
            if (CSS.ball.top <= 0 || CSS.ball.top >= CSS.arena.height - CSS.ball.height) {          //Upper and lower border crash check
                CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;                                     //changing ball direction
            }

            $('#pong-ball').css({top: CSS.ball.top,left: CSS.ball.left});

            if (CSS.ball.left <= CSS.stick.width && CSS.ball.top > CSS.stick1.top && CSS.ball.top < CSS.stick1.top + CSS.stick.height) {                                      //left stick crask control
            	CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1;
            }

            if (CSS.ball.left >= CSS.arena.width - CSS.ball.width - CSS.stick.width && CSS.ball.top > CSS.stick2.top && CSS.ball.top < CSS.stick2.top + CSS.stick.height) {   //rigth stick crash control
                CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1;
            }
            
            CSS.stick1.top = (CSS.ball.top -(CSS.stick1.top)*0.15);
            CSS.stick2.top = (CSS.ball.top -(CSS.stick2.top)*0.15);
    
            $('#stick-1').css("top", (CSS.stick1.top));
            $('#stick-2').css("top", (CSS.stick2.top));
            
            if ( CSS.ball.left + CSS.ball.width > CSS.arena.width ){
                CONSTS.score1++;
                $("#score-1")[0].innerHTML = (CONSTS.score1);
                if (CONSTS.score1 >= 5){  
                    alert(`COMPUTER Win!`);
                }
                roll();      
            }
 
            if ( CSS.ball.left < 0 ){ 
                CONSTS.score2++;
                $("#score-2")[0].innerHTML = (CONSTS.score2);
                if (CONSTS.score2 >= 5){
                    alert(`COMPUTER Win!`);
                }
                roll();
            }

        }, CONSTS.gameSpeed);
    }

    function roll() {
        CSS.ball.top = 250;
        CSS.ball.left = 350;

        var side = -1;

        if (Math.random() < 0.5) {
            side = 1;
        }

        CONSTS.ballTopSpeed = Math.random() * -2 - 3;
        CONSTS.ballLeftSpeed = side * (Math.random() * 2 + 3);
    }

    function saveGame() {
        localStorage.setItem("Score 1", CONSTS.score1);
        localStorage.setItem("Score 2", CONSTS.score2);
    }

    function readScores() {
        CONSTS.score1 = localStorage.getItem("Score 1");
        CONSTS.score2 = localStorage.getItem("Score 2");
    }

    start();
})();